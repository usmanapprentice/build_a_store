var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
	var _	= require("underscore");

	var id = req.query.id,
		title = req.query.title,
		description = req.query.description,
		db = req.db;

	var collection = db.collection('categories');
	collection.find({},function(err, items) {
		collection.find({"id":id},function(err, categories) {
			res.render("categories", {
				_	: _,
				// Template data
				items : items, // menu items
				categories: categories, //populating categories on the page.
				title : title,
				description : description,
				id: id
			});
		});
	});
});

module.exports = router;
