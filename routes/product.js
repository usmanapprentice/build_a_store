var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
	var _	= require("underscore");

	var id = req.query.id,
		title = req.query.title,
		description = req.query.description,
		name = req.query.name,
		db = req.db;

	var collection = db.collection('categories');
	collection.find({}, function(err, items) {
		var	products = db.collection('products');
		products.find({"id":id},function (err, product) {
			res.render("product", {
				_	: _,
				// Template data
				items : items, // menu items
				product: product, //populating categories on the page.
				title : title,
				description : description,
				id: id,
				name: name
			});
		});
	});
});

module.exports = router;
