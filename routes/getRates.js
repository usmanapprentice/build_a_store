var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
	var amount = req.query.amount,
		currencytoConvert = req.query.currencytoConvert,
		baseCurrency = "USD";
	var _	= require("underscore");
	const bnr = require("bnr");
	bnr.convert(amount, baseCurrency, currencytoConvert, function (err, amount, output) {
    if (err) { return console.error(err); }
    res.json(output);
	});
});

module.exports = router;
