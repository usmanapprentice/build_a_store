var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
	var _	= require("underscore");

	var db = req.db;
	var collection = db.collection('categories');
	collection.find({},function(err, items) {
		res.render("index", {
			_	: _,
			// Template data
			items : items,
			title : "J'aime la mode",
			description: "Buy online at J'aime la mode"
		});
	});
});

module.exports = router;
