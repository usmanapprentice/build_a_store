var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
	var _	= require("underscore");

	var id = req.query.id,
		title = req.query.title,
		description = req.query.description,
		db = req.db;

	var collection = db.collection('categories');
	collection.find( {}, function(err, items) {
		var	products = db.collection('products');
		products.find( {"primary_category_id":id}, function (err, products) {
			res.render("products", {
				_	: _,
				// Template data
				items : items, // menu items
				products: products, //populating categories on the page.
				title : title,
				description : description,
				id: id
			});
		});
	});
});

module.exports = router;
