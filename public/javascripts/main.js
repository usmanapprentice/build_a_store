$(document).ready(function () {
	var config = new App_config();

	// binding attributes to anchor texts.
	function breadcrumbProperty(el, link,  name, id, page_title, page_description) {
		el.text(name);
		el.attr('href', link + '?id=' + id + '&&title='+ page_title + '&&description='+ page_description);
	}

	// handling the breadcrumbs according to pages
	(function () {
		var pathname = window.location.pathname;
		var id = $('#page_id').val();
		$home = $('#navigator-home');
		$categories = $('#navigator-categories');
		$subcategories= $('#navigator-subcategories');
		$products = $('#navigator-products');
		$product = $('#navigator-product');
		// getting data for breadcrumbs navigation.
		$.ajax({
			type: 'GET',
			url: config.getUrl() + '/getNavigation',
			dataType: 'json',
			success: function (chunck) {
				if (pathname === '/subcategory') {
					for (props of chunck) {
						if (props.id === id) {
							breadcrumbProperty($categories, 'categories', props.name, props.id, props.page_title, props.page_title);
						}
					}
				}
				if (pathname === '/products' || pathname === '/feeling_red') {
					for (props of chunck) {
						for (property of props.categories) {
							for (prop of property.categories) {
								if (prop.id === id) {
									breadcrumbProperty($categories, 'categories', props.name, props.id, props.page_title, props.page_description);
									breadcrumbProperty($subcategories, 'subcategory', property.name, property.id, property.page_title, property.page_description);
								}
							}
						}
					}
				}

				if (pathname === '/product') {
					for (props of chunck) {
						for (property of props.categories) {
							for (prop of property.categories) {
								if (prop.id === id) {
									breadcrumbProperty($categories, 'categories', props.name, props.id, props.page_title, props.page_description);
									breadcrumbProperty($subcategories, 'subcategory', property.name, property.id, property.page_title, property.page_description);
									breadcrumbProperty($products, 'products', prop.name, prop.id, prop.page_title, prop.page_description);
								}
							}
						}
					}
				}

			},
			error: function (data) {
				console.log("Error.", data);
			}
		});
	})();


	$('.pill').css('border-radius', $('.pill').outerHeight()/2); // for elements in html assigned as class pill

	$('.img-zoom').hover(function() {
        $(this).addClass('transition');
    }, function() {
        $(this).removeClass('transition');
    });

	// for sliding images in product details page.
	$(function(){
    	$('.image-container img:gt(0)').hide();
    	setInterval(function(){$('.image-container :first-child').fadeIn().next('img').fadeOut().end().appendTo('.image-container');}, 3000);
	});

	// Currency change function.
	(function(){
		var itemPrice = $('#item-price').val(); //getting item's original price in USD.
		$('#currency-change').on('change', function () {
			var currency_code = $(this).val();

			// getting data from the bnr middleware.
			$.ajax({
				type: 'GET',
				url: config.getUrl() + '/getRates?currencytoConvert=' + currency_code + '&&amount=' + itemPrice,
				dataType: 'json',
				success: function (data) { //success getting currency conversion.
					var result = data.output;
					$('.money').text(result.currency+': '+ (+result.amount).toFixed(2));
				},
				error: function (data) { // error getting data from bnr.
					console.log("Error.", data);
				}
			});
		});
	})();

});
