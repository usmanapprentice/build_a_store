function App_config () {
	this.PROTOCOL = 'http';
    this.WEB_HOST = 'localhost';
    this.WEB_PORT = 80;
}

App_config.prototype.getUrl = function() {
    return this.PROTOCOL + '://' + this.WEB_HOST + ':' + this.WEB_PORT;
};
